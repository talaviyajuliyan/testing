<?php

$conn=mysqli_connect('localhost','phpmyadmin','root');
mysqli_select_db($conn,'training');

?>

<!DOCTYPE html>
<html>
<head>
	<title>Registration Form</title>
	<link rel="stylesheet" type="text/css" href="main.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript" src="form-validation.js"></script>

	<script src="https://cdn.jsdelivr.net/jquery/1.12.4/jquery.min.js"></script>
  	<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
  	<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/additional-methods.min.js"></script>
	<script>
      function change_country()
      {
      var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                  document.getElementById("state").innerHTML =
                  this.responseText;
              }
          	};
      		xmlhttp.open("GET","ajax.php?country="+document.getElementById("ctry").value,true);
      		xmlhttp.send();
      }

      function change_state()
      {
        var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                  document.getElementById("ct").innerHTML =
                  this.responseText;
               }
          	};
      		xmlhttp.open("GET","ajax.php?state="+document.getElementById("state").value,true);
      		xmlhttp.send();
      }
  	</script>


</head>
<body>
	<div class="container">
		<h1>Registration Form</h1>	
	

	<div class="form">
		<fieldset>
			<form action="server.php" method="POST" id="registration">

			<div class="a">
			<label for="username">Username: 
			<input type="text" name="username" id="form_user">
			</label><span id="user_error"></span>
			</div>
			
			<div class="a">
				<label for="password">Password: 
				<input type="password" name="psw1" id="form_psw1">
				</label><span id="psw1_error"></span>
			</div>

			<div class="a">
				<label for="conform">Conform Password: 
				<input type="password" name="psw2" id="form_psw2">
				</label><span id="psw2_error"></span>
			</div>

			<div class="a">
				<label for="country">Country:
				  	<select name="country" id="ctry" onClick="change_country()">
				        <option value="Select country">Select Country</option>
						  	<?php
						        $res=mysqli_query($conn, "SELECT * FROM `country`");
						        while($row=mysqli_fetch_array($res)){
						    ?>
						        <option value="<?php echo $row['country_id'];?>"> <?php echo $row['name'];?></option>
						    <?php
						        }
						    ?>
					</select>
				</label><span id="country_error"></span>
			</div>

			<div class="a">
				<label for="state">State: 
					<select name="state" id="state" onclick="change_state()">
						<option value="Select state">Select State</option>

					</select>
					</label><span id="state_error"></span>
			</div>

			<div class="a">
				<label for="city">City: 
				<select name="city" id="ct">
					<option value="Select city">Select City</option>

				</select>
				</label><span id="city_error"></span>
			</div>

			<div class="a">
				<label for="hobbie">Hobbies: 
				<input type="checkbox"  name="hobi[]" value="Reading">Reading Books
				<input type="checkbox"  name="hobi[]" value="Coding">Play with code
				<input type="checkbox" name="hobi[]" value="Singing">Singing
				<input type="checkbox" name="hobi[]" value="Dancing">Garba
				</label><span id="hobi_error"></span>
			</div>

			<div class="a">
				<label for="Gender">Gender: 
				<input type="radio" id="gender" name="gender" value="male">Male
				<input type="radio" name="gender" value="female">Female
				</label><span id="gender_error"></span>
			</div>

			<div class="a">
				<input type="submit" name="submit" id="sm" value="submit">
			</div>
			</form>
		</fieldset>
	</div>
</div>

</body>