/*
 $(document).ready(function(e){
    jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
    }); 



$("#registration").validate({
      
      rules: {

          username:{
            required:true
            },
          
          psw1:{
            required: true,
            minlength: 5
            },

          psw2:{
            required: true,
            minlength: 5
            },
          country: {
            required: {
              depends: function(element) {
               return $("#ctry").val() == "";
              },
             }
            },
            state: {
            required: {
              depends: function(element) {
               return $("#state").val() == "";
              },
             }
            },
            city: {
            required: {
              depends: function(element) {
               return $("#ct").val() == "";
              },
             }
            },
          'hobi[]':{
            required:true
            },
          gender:{
            required:true
            },
          
        },
  

      messages: {

          username: {
            required: "Please enter your username"
            },
          psw1: {
            required: "Please enter your password"
            },
          psw2: {
            required: "Please enter the same password"
            },
          country: {
            required: "Please select an option from the list",
            },
            state: {
            required: "Please select an option from the list",
            },
            city: {
            required: "Please select an option from the list",
            },
          'hobi[]': {
            required: "Checked at least one checkbox"
            },
        },
  
      submitHandler: function(form) {
      form.submit();
        },
  });

});*/





$(function(){

    $("#user_error").hide();
    $("#psw1_error").hide();
    $("#psw1_error").hide();
    $("#country_error").hide();
    $("#state_error").hide();
    $("#city_error").hide();
    $("#hobi_error").hide();
    $("#gender_error").hide();

    var user = false;
    var pass1 = false;
    var pass2= false;
    var country = false;
    var state = false;
    var city= false;
    var hobi = false;
    var gender= false;

    $("#form_user").focusout(function(){

      check_user();

    });
    $("#form_psw1").click(function(){
      
      check_pass1();
      
    });
    $("#form_psw2").click(function(){
      check_pass2();
      
    });
    $("#sm").click(function(){
      check_country();
      
    });
    $("#sm").click(function(){
      check_state();
      
    });
    $("#sm").click(function(){
      check_city();
      
    });
    $("#sm").click(function(){
      check_hobi();
      
    });
    $("#sm").click(function(){
      check_gender();
      
    });

    function check_user(){
      var user_length = $("#form_user").val().length;

      if(user_length<1){
        $("#user_error").html("Please Enter Username ");
        $("#user_error").show();
        user=true;
      } else{
        $("#user_error").hode();
      }
    }

    function check_pass1(){
      var psw_length = $("#form_psw1").val().length;

      if(psw_length<5){
        $("#psw1_error").html("At least 5 character");
        $("#psw1_error").show();
        pass1=true;
      }else{
        $("#psw1_error").hide();
      }
    }

    function check_pass2(){
      var psw1 = $("#form_psw1").val();
      var psw2 = $("#form_psw2").val();
      if(psw1 != psw2){
        $("#psw2_error").html("Passwords don't match");
        $("#psw2_error").show();
        pass2=true;
      }else{
        $("#psw2_error").hide();
      }
    }

    function check_country(){
      var country = $("#ctry").val();

      if(country =="Select country"){
        $("#country_error").html("Please Select Country");
        $("#country_error").show();
        country= true;
      }else{
        $("$country_error").hide();
      }
    }

    function check_state(){
      var state = $("#state").val();

      if(state =="Select state"){
        $("#state_error").html("Please Select State");
        $("#state_error").show();
        state= true;
      }else{
        $("$state_error").hide();
      }
    }

    function check_city(){
      var city = $("#ct").val();

      if(city =="Select city"){
        $("#city_error").html("Please Select City");
        $("#city_error").show();
        city=true;
      }else{
        $("$city_error").hide();
      }
    }

    function check_hobi(){

      if($('input[type=checkbox]:checked').length == 0)
    {
    $("#hobi_error").html("Please select at least one checkbox" );
    $("#hobi_error").show();
    hobi=true;
    }else{
      $("#hobi_error").hide();
    }
    }

    function check_gender(){
      var isChecked = $('input[type="radio"]:checked');
     if(!isChecked.length>0){
         $("#gender_error").html('Nothing Selected');
         $("#gender_error").show();
         gender=true;
     }else{
         $("#gender_error").hide();
     }
    }


    $("#registration").submit(function(){
      user =false;
      pass1= false;
      pass2 = false;
      country = false;
      state =false;
      city = false;
      hobi = false;
      gender = false;

      check_user();
      check_pass1();
      check_pass2();
      check_country();
      check_state();
      check_city();
      check_hobi();
      check_gender();

      if(user == false && pass1==false && pass2 == false && country==false && state==false && city==false && hobi==false && gender==false){
        return true;
      }else{
        return false;
      }
    })
});

    $(function(e){
      $("#login-btn").click(function(){
        var username = $("#username").val();
        var password = $("#password").val();

        $.post("login1.php",{username: username, password: password})
        .done(function(data){
          if(data == "success"){
            window.location = "list.php";
          }else{
            $("#login_message").text("Invalid Username or Password! Try agian.");
            $("#login").show();
          }
        });
      });

    });
