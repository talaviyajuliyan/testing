<?php include('session.php'); ?>

<!DOCTYPE html>
<html>
<head>
	
	<title>Login Page</title>
	<link rel="stylesheet" type="text/css" href="main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$('#login').click(function(){
				
				var user = $('#user').val();
				var password = $('#pass').val();

				if(user==''||password == ''){
					alert("Please Fill All fields");
				
				}
			});

		
		

		});
	</script>

</head>
<body>
<div class="container">
	<h1>Login Page</h1>
	<div class="form">
		<fieldset>
			
			<form action="list1.php" method="post" id="frmLogin">
				<span class="error"></span>
				<div class="error-message"><?php if(isset($message)) { echo $message; } ?></div>	
				<div class="a">
					<label for="login">Username: </label>
					<input name="username" type="text" class="input-field" id="user" >
				</div>
				<div class="a">
					<label for="password">Password: </label>
					<input name="password" id="pass" type="password" class="input-field" >
				</div>

				<div class="a">

					<input type="submit" id="login" name="login" value="Login" class="form-submit-button"></span>
				</div>  
				<div class="a">
					<h4>New Registration</h4>
					<button type="button" id="r_page" name="register_page"><a href="register.php" style="text-decoration: none;color: #ffffff;">Register</button>
				</div>     
			</form>
	
		</fieldset>
	</div>
</div>

</body>
</html>